package com.wangshi.MyOpenglTest;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class MyGLSurfaceview extends GLSurfaceView {
    public MyGLSurfaceview(Context context) {
        super(context);
    }

    public MyGLSurfaceview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
