package com.wangshi.MyOpenglTest;

import android.opengl.GLES10;
import android.opengl.GLSurfaceView;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public abstract class AbstractRender implements GLSurfaceView.Renderer {
    public float ratio;
    private boolean is_GL_COLOR_ARRAY=true;//是否启用颜色缓冲区 默认启用，不启用，设置为false
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //设置清屏色，即绘图的背景色，一般设置为黑色,取值都是0-1.
        gl.glClearColor(0,0,0,1);
        //设置清除模板缓冲区的值
        gl.glClearStencil(0);
        //启用顶点缓存，不启用会不显示
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        //启用颜色缓冲区 ，没有使用颜色缓冲时，不能启用，否则看不到绘制的内容
        if(is_GL_COLOR_ARRAY){
            gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
        }
        //启用深度测试
        gl.glEnable(GL10.GL_DEPTH_TEST);//启用后被挡住的物体看不见，否则能看到物体背后的物体
        boolean isCull=false;//是否表面剔除
        //绘制时，逆时针绘制，绘制的是正面。顺时针吗绘制，绘制的是背面
        if(isCull){
            //启用表面剔除 当物体不动，则应当不绘制，将其剔除
            gl.glEnable(GL10.GL_CULL_FACE);
            //指定前面
            //ccw:counter clock wise-->逆时针方向为正面
            //cw：clock wise-->顺时针方向为正面
            gl.glFrontFace(GL10.GL_CW);
            //剔除背面
            gl.glCullFace(GL10.GL_BACK);
        }
        //启用裁剪测试，指定刷新区域
        gl.glEnable(GL10.GL_SCISSOR_TEST);
        //启用模板缓冲区
        /**
         * 模板测试函数
         * gl.glStencilFunc(func,ref,mask);在调用之前必需先调用 gl.glClearStencil();
         * func：测试函数,可取下面的值:GL10.GL_NEVER(从不通过)，GL10.GL_ALWAYS(总是通过)，GL10.GL_LESS(A<B时通过),
         * GL10.GL_LEQUAL(A<=B时通过),GL10.GL_EQUAL(A=B时通过),GL10.GL_GEQUAL(A>=B时通过),GL10.GL_GREATER(A>B时通过)
         * GL10.GL_NOTEQUAL(A!=B时通过)
         * ref:参考值
         * mask:设置一个掩码，它将会与参考值和储存的模板值在测试比较它们之前进行与(AND)运算。初始情况下所有位都为1。
         * 模板缓冲的深度在1-8位之间，传递给reg或mask的值，超过了有效深度，那么它们会被截断，只使用那些位于低位的有效位
         *
         * 进入模板缓冲区的值取决于glStencilOp是如何被调用的。
         * fail:模板测试失败采取的行为
         * zfail:模板测试通过，但深度测试失败时采取的行为。
         * zpass:模板测试和深度测试都通过时采取的行为
         * gl.glStencilOp(fail,zfail,zpass);这些值告诉opengl,如果模板测试失败，它应如何修改模板缓冲区的值
         * fail 模板值测试失败，zfail 深度值测试失败，zpass深度值测试通过
         * 这些参数的合法值包括:GL10.GL_KEEP(保持当前值),GL10.GL_ZERO(把它设置为0),GL10.GL_REPLACE(用参考值代替，取自glStencilFunc),
         * GL10.GL_INCR(增加这个值),GL10.GL_DECR(减少这个值),GL10.GL_INVERT(反转这个值),GL10.GL_INCR_WRAP(循环增加这个值)
         * GL10.GL_DECR_WRAP(循环减少这个值)
         */
        gl.glEnable(GL10.GL_STENCIL_TEST);

        //启用纹理
        gl.glEnable(GL10.GL_TEXTURE_2D);
        //启用纹理顶点缓冲区
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        /**
         * 设置视口，即视图的窗口,一般取sufaceview的宽和高,即输出画面的区域
         * 也可以设置，width，height以外的宽高
         */
        gl.glViewport(0,0,width,height);
        //设置投影矩阵模式（必须设置平截投体前设置） 1.透视投影，远小近大，有深度 2.正交投影 与远近无关，远近都一样大
        gl.glMatrixMode(GL10.GL_PROJECTION);
        //加载单位矩阵
        gl.glLoadIdentity();
        //设置平截投体，即视景体。摄像机的可观测区域空间
        ratio = (float)height/(float)width;//为了显示渲染后不失真，保持和视口宽高比例相同
        /**
         * 一般把平截投体的中心作为原点，如果是在一个平面上显示，可将原点放在近景面，方便计算绘制图像在平面中的位置
         *  float left,
         *  float right,
         *  float bottom,
         *  float top,
         *  float zNear 近景面 距离摄像机位置
         *  float zFar 远景面 距离摄像机位置
         */
        gl.glFrustumf(-1,1,-1*ratio,1*ratio,3,7);
        boolean isScissor=false;//是否指定刷新区域
        if(isScissor){
            //裁剪测试 指定刷新区域
            gl.glScissor(0,0,width/2,height);
        }
    }
    public void setIs_GL_COLOR_ARRAY(boolean is_GL_COLOR_ARRAY) {
        this.is_GL_COLOR_ARRAY = is_GL_COLOR_ARRAY;
    }
    public abstract void onDrawFrame(GL10 gl) ;

}
