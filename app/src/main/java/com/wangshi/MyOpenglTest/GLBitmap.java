package com.wangshi.MyOpenglTest;

import java.nio.ByteBuffer;

import java.nio.ByteOrder;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;

import android.graphics.Bitmap;

import android.graphics.BitmapFactory;

import android.opengl.GLUtils;
public class GLBitmap {

    private FloatBuffer textureBuffer; // buffer holding the texture coordinates
    //标准纹理坐标 整张图：宽度从0->1,高度从0->1; 可以通过纹理坐标截取图片的任意位置，从而实现裁剪效果
    private float texture[] = {
            // Mapping coordinates for the vertices
            0.0f, 1.0f, // top left (V2)
            0.0f, 0.0f, // bottom left (V1)
            1.0f,1.0f, // top right (V4)
            1.0f, 0.0f // bottom right (V3)
    };
    private FloatBuffer vertexBuffer; // buffer holding the vertices din

    //标准顶点坐标 这里将宽的顶点定为：-1到1，实际上，高度应该根基图片宽高比例计算得出
    private float vertices[] = { -1.0f, -1.0f, 0.0f, // V1 - bottom left
            -1.0f, 1.0f, 0.0f, // V2 - top left
            1.0f, -1.0f, 0.0f, // V3 - bottom right
            1.0f, 1.0f, 0.0f // V4 - top right
    };
    public GLBitmap(Context context) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.test);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        bitmap.recycle();
        bitmap = null;

        //为了防止图片加载变形，重设顶点坐标
        resetVertices(bitmapWidth,bitmapHeight);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuffer.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.position(0);
        byteBuffer = ByteBuffer.allocateDirect(texture.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        textureBuffer = byteBuffer.asFloatBuffer();
        textureBuffer.put(texture);
        textureBuffer.position(0);
    }

    /** The texture pointer */

    private int[] textures = new int[1];//1表示需要生成一个纹理
    //加载纹理
    public void loadGLTexture(GL10 gl, Context context) {
        //生成bitmap
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.test);
        //生成纹理对象(指针)
        gl.glGenTextures(1, textures, 0);
        //绑定纹理到指定的纹理目标类型上
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
        // 设置纹理过滤参数
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,GL10.GL_LINEAR);
        // 使用Android GLUtils指定二维纹理图像
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
        //图像已经绑定到纹理，可以回收，以免占用内存
        bitmap.recycle();
    }

    public void draw(GL10 gl) {
//        //
//        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
        // 开启缓冲区
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        // 设置面旋转
        //GL_CCW 表示窗口坐标上投影多边形的顶点顺序为逆时针方向的表面为正面。
        //GL_CW 表示顶点顺序为顺时针方向的表面为正面。
        gl.glFrontFace(GL10.GL_CW);
        //设置顶点坐标
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
        //将顶点绘制为三角形
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
        // 离开禁用缓冲区
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    }
    private void resetVertices(int bitmapWidth,int bitmapHeight){
        float heightScale = bitmapHeight*1.0f/bitmapWidth;
        vertices= new float[]{ -1.0f, -1.0f*heightScale, 0.0f, // V1 - bottom left
                -1.0f, 1.0f*heightScale, 0.0f, // V2 - top left
                1.0f, -1.0f*heightScale, 0.0f, // V3 - bottom right
                1.0f, 1.0f*heightScale, 0.0f // V4 - top right
        };
    }
}
