package com.wangshi.MyOpenglTest.example06;

import android.opengl.GLU;
import android.util.FloatMath;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 *  绘制球体
 */
public class MyRender06 extends AbstractRender {

    private float roate=0;
    private float rotateStep=0.2f;//每次跳转旋转角度的固定大小
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)
        gl.glShadeModel(GL10.GL_FLAT);
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);
        gl.glRotatef(roate,0,1,0);//绕y轴旋转
        gl.glRotatef(30,1,0,0);
        roate = roate+rotateStep;
//        /******************************绘制白色螺旋线**************************************/
//        gl.glPushMatrix();
//        gl.glRotatef(xrotate,1,0,0);//绕x轴旋转
//        gl.glRotatef(yrotate,0,1,0);//绕y轴旋转
//        vertexList = new ArrayList<>();
//        float x=0,y=0.8f,z=0,ystep=0.005f,r=0.7f;
//        for(float angle=0;angle<(Math.PI*2*3);angle+=(Math.PI/40)){
//            x = (float)(r*Math.cos(angle));
//            z = -(float)(r*Math.sin(angle));
//            y = y-ystep;
//            vertexList.add(x);
//            vertexList.add(y);
//            vertexList.add(z);
//        }
//        ByteBuffer fvb = BufferUtil.list2ByteBuffer(vertexList);
//        //绘制白色直线，设置模板函数，所有操作都不能通过测试，但是对模板缓冲区的值进行增加
//        gl.glStencilFunc(GL10.GL_EQUAL,1,0);
//        //进入模板缓冲区的值取决于glStencilOp是如何被调用的。
//        gl.glStencilOp(GL10.GL_INCR,GL10.GL_INCR,GL10.GL_INCR);
//        //绘制白色螺旋线
//        gl.glColor4f(1,1,1,1);
//        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
//        gl.glVertexPointer(3,GL10.GL_FLOAT,0,fvb);
//        //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
//        gl.glDrawArrays(GL10.GL_LINE_STRIP,0,vertexList.size()/3);//除以3，是因为3个float表示一个点
//        gl.glPopMatrix();
//        /******************************绘制红色方块**************************************/
//
//        if(xadd){
//            left = left+0.01f;
//        }else{
//            left = left-0.01f;
//        }
//        if(left<-ratio){
//            xadd = true;
//        }
//        if(left>=ratio-width){
//            xadd = false;
//        }
//        if(yadd){
//            top = top+0.01f;
//        }else{
//            top = top-0.01f;
//        }
//        if(top>=1){
//            yadd= false;
//        }
//        if(top<-1+width){
//            yadd = true;
//        }
//        float[] rectVertex = {left,top-width,2f,left,top,2f,
//        left+width,top-width,2f,left+width,top,2f};
//        ByteBuffer vbb = BufferUtil.arr2ByteBuffer(rectVertex);
//        //设置模板函数，所有操作都不能通过测试，但是对模板缓冲区的值进行增加
//        gl.glStencilFunc(GL10.GL_EQUAL,1,0);
//        //进入模板缓冲区的值取决于glStencilOp是如何被调用的。
//        gl.glStencilOp(GL10.GL_KEEP,GL10.GL_KEEP,GL10.GL_KEEP);
//        //绘制红色方块
//        gl.glColor4f(1,0,0,1);
//        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
//        gl.glVertexPointer(3,GL10.GL_FLOAT,0,vbb);
//        //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
//        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,rectVertex.length/3);//除以3，是因为3个float表示一个点


        //计算球体坐标
        float R = 0.7f;//球半径

        int stack=8;//水平层数
        float stackStep = (float) Math.PI/stack;

        int slice = 12;//水平圆上，点的个数
        float sliceStep = (float) Math.PI/slice;

        float r0,r1,y0,y1,x0,x1,z0,z1;
        float alpha0=0;
        float alpha1=0;
        float bata = 0;
        ArrayList<Float> coordList = new ArrayList<>();
        for(int i=0;i<stack;i++){
            alpha0 =(float) (-Math.PI/2+i*stackStep);
            alpha1 =(float) (-Math.PI/2+(i+1)*stackStep);
            y0 = (float) (R* Math.sin(alpha0));
            r0 =(float) (R*Math.cos(alpha0));
            y1 = (float) (R*Math.sin(alpha1));
            r1 =(float) (R*Math.cos(alpha1));
            for(int j=0;j<=slice*2;j++){
                bata = j*sliceStep;
                x0=(float) (r0*Math.cos(bata));
                z0=(float) (-r0*Math.sin(bata));
                x1=(float) (r1*Math.cos(bata));
                z1=(float) (-r1*Math.sin(bata));
                coordList.add(x0);
                coordList.add(y0);
                coordList.add(z0);
                coordList.add(x1);
                coordList.add(y1);
                coordList.add(z1);
            }

        }
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = BufferUtil.list2ByteBuffer(coordList);
        //设置绘制颜色
        gl.glColor4f(1,0,0,1);
        //设置点大小
        gl.glPointSize(1);//不设最默认为1像素大小
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
        //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
        gl.glDrawArrays(GL10.GL_LINE_STRIP,0,coordList.size()/3);//除以3，是因为3个float表示一个点
    }
}
