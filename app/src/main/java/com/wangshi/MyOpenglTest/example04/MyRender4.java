package com.wangshi.MyOpenglTest.example04;

import android.opengl.GLU;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

/**
 *  简单绘 深度测试，表面剔除和裁剪
 *  绘制圆锥
 */
public class MyRender4 extends AbstractRender {
    private float roate=0;
    private float rotateStep=0.2f;//每次跳转旋转角度的固定大小
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)
        gl.glShadeModel(GL10.GL_FLAT);
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);
        //设置旋转
//        gl.glRotatef(90,0,1,0);//绕y轴旋转
        gl.glRotatef(roate,0,1,0);//绕y轴旋转
        gl.glRotatef(30,1,0,0);
        roate = roate+rotateStep;
        //得到坐标
        //计算点坐标
        float r = 0.5f;
        float x =0,y=0,z=-1f;
        /***************锥面******************/
        List<Float> coordList  = new ArrayList<Float>();
        //添加锥顶点
        coordList.add(0.0f);
        coordList.add(0.0f);
        coordList.add(0.5f);
        //顶点颜色集合，得到颜色坐标
        List<Float> colorlist = new ArrayList<>();
        //锥顶点颜色
        colorlist.add(1f);//r
        colorlist.add(0f);//g
        colorlist.add(0f);//b
        colorlist.add(1f);//a
        boolean flag = false;

        int count = 0;
        //锥面
        for(float alpha=0;alpha<=Math.PI*2+Math.PI/16;alpha=(float)(alpha+Math.PI/16)){
            x = (float)(r*Math.cos(alpha));
            y = (float)(r*Math.sin(alpha));
            coordList.add(x);
            coordList.add(y);
            coordList.add(z);
            count++;
            if(!flag){
                colorlist.add(1f);//r
                colorlist.add(1f);//g
                colorlist.add(0f);//b
                colorlist.add(1f);//a
                flag = true;
            }else{
                colorlist.add(1f);//r
                colorlist.add(0f);//g
                colorlist.add(0f);//b
                colorlist.add(1f);//a
                flag = false;
            }
        }
        //在颜色缓冲区增加一个颜色
        if(count%2==0){
            colorlist.add(1f);//r
            colorlist.add(1f);//g
            colorlist.add(0f);//b
            colorlist.add(1f);//a
        }else{
            colorlist.add(1f);//r
            colorlist.add(0f);//g
            colorlist.add(0f);//b
            colorlist.add(1f);//a
        }
        /***************锥底面******************/
        List<Float> coordBottomList  = new ArrayList<Float>();
        //添加锥底面中心点
        coordBottomList.add(0.0f);
        coordBottomList.add(0.0f);
        coordBottomList.add(-1f);
        for(float alpha=0;alpha<=Math.PI*2+Math.PI/16;alpha=(float)(alpha+Math.PI/16)){
            x = (float)(r*Math.cos(alpha));
            y = (float)(r*Math.sin(alpha));
            coordBottomList.add(x);
            coordBottomList.add(y);
            coordBottomList.add(z);
        }
        /***************绘制锥面***/
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = BufferUtil.list2ByteBuffer(coordList);
        //设置绘制颜色
//        gl.glColor4f(1,0,0,1);
        //使用颜色缓冲区 值4表示4个值表示一个颜色，0跨度
        gl.glColorPointer(4,GL10.GL_FLOAT,0,BufferUtil.list2ByteBuffer(colorlist));
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
        //GL10.GL_TRIANGLES 绘制扇面,first 起始位置，count 点的数量
        gl.glDrawArrays(GL10.GL_TRIANGLE_FAN,0,coordList.size()/3);//除以3，是因为3个float表示一个点

        /***************绘制锥底面***/
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb2 = BufferUtil.list2ByteBuffer(coordBottomList);
        //使用颜色缓冲区 值4表示4个值表示一个颜色，0跨度
        ByteBuffer colorBuffer = BufferUtil.list2ByteBuffer(colorlist);
        colorBuffer.position(16);//将颜色移动一个颜色，4个float 表示一个颜色，故位置从移动16个byte,注意position表示：从第几个字节开始
        gl.glColorPointer(4,GL10.GL_FLOAT,0,colorBuffer);
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb2);

        //GL_TRIANGLE_FAN 绘制扇面
        /**
         * GL_TRIANGLES：每三个顶点绘制一个三角形，如果顶点数量不是3的倍数，则忽略最后一个或两个顶点。
         *
         * GL_TRIANGLE_STRIP：有两种情况，
         * （1）当前顶点序号n是偶数时，三角形三个顶点的顺序是(n - 2, n - 1, n )。
         * （2）当前顶点序号n是奇数时，三角形三个顶点的顺序是(n - 1, n - 2, n)。
         * 这两种情况，保证了采用此种渲染方式的三角形顶点的卷绕顺序。
         * 例如：对于v2顶点，其序号为2，此时三个顶点的顺序是(v0, v1, v2)；对于v3顶点，其序号为3，此时三个顶点的顺序是(v2, v1, v3)，均是逆时针卷绕。
         * GL_TRIANGLE_FAN：一系列顶点中的第一个点为中心点，其他顶点为边缘点，绘制一系列组成扇形的相邻三角形。例如三角形(v0, v1, v2)、(v0, v2, v3)。
         */
        gl.glDrawArrays(GL10.GL_TRIANGLE_FAN,0,coordBottomList.size()/3);//除以3，是因为3个float表示一个点
    }
}
