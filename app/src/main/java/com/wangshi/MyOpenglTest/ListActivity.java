package com.wangshi.MyOpenglTest;

import android.content.Intent;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.wangshi.MyOpenglTest.exameple01.Activity01;
import com.wangshi.MyOpenglTest.example02.Activity02;
import com.wangshi.MyOpenglTest.example03.Activity03;
import com.wangshi.MyOpenglTest.example04.Activity04;
import com.wangshi.MyOpenglTest.example05.Activity05;
import com.wangshi.MyOpenglTest.example06.Activity06;
import com.wangshi.MyOpenglTest.example07.Activity07;
import com.wangshi.MyOpenglTest.example08.Activity08;
import com.wangshi.MyOpenglTest.example09.Activity09;
import com.wangshi.MyOpenglTest.example10.Activity10;

import androidx.appcompat.app.AppCompatActivity;

public class ListActivity extends AppCompatActivity {

    private Button opengl_base,opengl_point,opengl_line,opengl_depath,
            opengl_stencil,opengl_sphere,opengl_ring,opengl_texture,opengl_squre,
            opengl_flight,opengl_texture_shader,opengl_texture_shader2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
       init();
        if(!PermissionUtil.android11()){//小于android 11
            initPermission();
        }else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                if (!Environment.isExternalStorageManager()) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                    startActivityForResult(intent, 10020);
                }else{
                    initPermission();
                }
            }
        }
    }
    private void init(){
        getAllViews();
        setListener();
    }
    private void getAllViews(){
        opengl_base =findViewById(R.id.opengl_base);
        opengl_point =findViewById(R.id.opengl_point);
        opengl_line  = findViewById(R.id.opengl_line);
        opengl_depath = findViewById(R.id.opengl_depath);
        opengl_stencil = findViewById(R.id.opengl_stencil);
        opengl_sphere = findViewById(R.id.opengl_sphere);
        opengl_ring = findViewById(R.id.opengl_ring);
        opengl_texture = findViewById(R.id.opengl_texture);
        opengl_squre = findViewById(R.id.opengl_squre);
        opengl_flight = findViewById(R.id.opengl_flight);
        opengl_texture_shader = findViewById(R.id.opengl_texture_shader);
        opengl_texture_shader2 = findViewById(R.id.opengl_texture_shader2);

    }
    private void setListener(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.opengl_base:
                        jumpToBase();
                        break;
                    case R.id.opengl_point:
                        jumpToPoint();
                        break;
                    case R.id.opengl_line:
                        jumpToline();
                        break;
                    case R.id.opengl_depath:
                        jumpToDepath();
                        break;
                    case R.id.opengl_stencil:
                        jumpToStencil();
                        break;
                    case R.id.opengl_sphere:
                        jumpToSphere();
                        break;
                    case R.id.opengl_ring:
                        jumpToRing();
                        break;
                    case R.id.opengl_texture:
                        jumpToTexture();
                        break;
                    case R.id.opengl_squre:
                        jumpToSqure();
                        break;
                    case R.id.opengl_flight:
                        jumpToFlight();
                        break;
                    case R.id.opengl_texture_shader:
                        jumpToShader();
                        break;
                    case R.id.opengl_texture_shader2:
                        jumpToShader2();
                        break;
                }
            }
        };
        opengl_base.setOnClickListener(listener);
        opengl_point.setOnClickListener(listener);
        opengl_line.setOnClickListener(listener);
        opengl_depath.setOnClickListener(listener);
        opengl_stencil.setOnClickListener(listener);
        opengl_sphere.setOnClickListener(listener);
        opengl_ring.setOnClickListener(listener);
        opengl_texture.setOnClickListener(listener);
        opengl_squre.setOnClickListener(listener);
        opengl_flight.setOnClickListener(listener);
        opengl_texture_shader.setOnClickListener(listener);
        opengl_texture_shader2.setOnClickListener(listener);


    }
    private void jumpToBase(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity01.class);
        startActivity(in);
    }
    private void jumpToPoint(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity02.class);
        startActivity(in);
    }
    private void jumpToline(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity03.class);
        startActivity(in);
    }
    private void jumpToDepath(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity04.class);
        startActivity(in);
    }
    private void jumpToStencil(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity05.class);
        startActivity(in);
    }
    private void jumpToSphere(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity06.class);
        startActivity(in);
    }
    private void jumpToRing(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity07.class);
        startActivity(in);
    }
    private void jumpToTexture(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity08.class);
        startActivity(in);
    }
    private void jumpToSqure(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity09.class);
        startActivity(in);
    }
    private void jumpToFlight(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, Activity10.class);
        startActivity(in);
    }
    private void jumpToShader(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, com.wangshi.MyOpenglTest.example11.Activity08.class);
        startActivity(in);
    }
    private void jumpToShader2(){
        Intent in= new Intent();
        in.setClass(ListActivity.this, com.wangshi.MyOpenglTest.example12.Activity12.class);
        startActivity(in);
    }
    private void initPermission() {
        boolean isGetStorePermission = PermissionUtil.isHavePermission(this, PermissionUtil.TYPE.STORAGE);
        if (!isGetStorePermission) {
            PermissionUtil.startRequestPermission(this, PermissionUtil.TYPE.STORAGE);
        } else {
        }
    }
}
