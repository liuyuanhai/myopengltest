package com.wangshi.MyOpenglTest.example03;



import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wangshi.MyOpenglTest.MyGLSurfaceview;
import com.wangshi.MyOpenglTest.example02.MyRender02;

/**
 * 简单绘制线
 */
public class Activity03 extends AppCompatActivity {

    private MyGLSurfaceview glSurfaceView;
    private MyRender03 myRender03;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glSurfaceView = new MyGLSurfaceview(this);
        //设置渲染器
        myRender03 = new MyRender03();
        myRender03.setIs_GL_COLOR_ARRAY(false);
        glSurfaceView.setRenderer(myRender03);
        /**
         *  设置渲染模式
         *  1.GLSurfaceView.RENDERMODE_CONTINUOUSLY 持续渲染渲染（默认）
         *  2.GLSurfaceView.RENDERMODE_WHEN_DIRTY 脏渲染,命令渲染（请求时渲染）
         *    使用glSurfaceView.requestRender();发出渲染命令
         */
        setContentView(glSurfaceView);
    }
}
