package com.wangshi.MyOpenglTest.example03;

import android.opengl.GLU;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

/**
 *  * 画线有三种模式
 *  * 1.lines 线段集合 例如：abcd四个点， 则绘制 a到b和c到d
 *  * 2.line_strip 线带  例如：abcd四个点， 则绘制 a到b,b到c,c到d
 *  * 3.line_loop 线管 例如：abcd四个点， 则绘制 a到b,b到c,c到d,d到a
 */
public class MyRender03 extends AbstractRender {
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);
        //得到坐标
        //计算点坐标
        float r = 0.5f;
        List<Float> coordList  = new ArrayList<Float>();
        //点1
        coordList.add(0.5f);
        coordList.add(0.5f);
        coordList.add(0.0f);
        //点2
        coordList.add(0.5f);
        coordList.add(-0.5f);
        coordList.add(0.0f);
        //点3
        coordList.add(-0.5f);
        coordList.add(-0.5f);
        coordList.add(0.0f);
        //点4
        coordList.add(-0.5f);
        coordList.add(0.5f);
        coordList.add(0.0f);
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = BufferUtil.list2ByteBuffer(coordList);
        //设置绘制颜色
        gl.glColor4f(1,0,0,1);
        gl.glLineWidth(2);//设置线宽
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
        /**
         * GL10.GL_LINES 绘制线
         * 1.GL_LINES 线段集合 例如：abcd四个点， 则绘制 a到b和c到d
         * 2.GL10.GL_LINE_STRIP 线带  例如：abcd四个点， 则绘制 a到b,b到c,c到d
         * 3.GL_LINE_LOOP 线管 例如：abcd四个点， 则绘制 a到b,b到c,c到d,d到a
         */
        gl.glDrawArrays(GL10.GL_LINE_LOOP,0,coordList.size()/3);//除以3，是因为3个float表示一个点
    }
}
