package com.wangshi.MyOpenglTest.example02;



import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wangshi.MyOpenglTest.MyGLSurfaceview;
import com.wangshi.MyOpenglTest.exameple01.MyRender01;

/**
 * 简单绘制螺旋的点
 */
public class Activity02 extends AppCompatActivity {

    private MyGLSurfaceview glSurfaceView;
    private MyRender02 myRender02;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glSurfaceView = new MyGLSurfaceview(this);
        //设置渲染器
        myRender02 = new MyRender02();
        myRender02.setIs_GL_COLOR_ARRAY(false);
        glSurfaceView.setRenderer(myRender02);
        /**
         *  设置渲染模式
         *  1.GLSurfaceView.RENDERMODE_CONTINUOUSLY 持续渲染渲染（默认）
         *  2.GLSurfaceView.RENDERMODE_WHEN_DIRTY 脏渲染,命令渲染（请求时渲染）
         *    使用glSurfaceView.requestRender();发出渲染命令
         */
        setContentView(glSurfaceView);
    }
}
