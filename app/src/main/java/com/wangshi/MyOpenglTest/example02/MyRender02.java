package com.wangshi.MyOpenglTest.example02;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyRender02 extends AbstractRender {
    private float roate=0;
    private float rotateStep=1f;//每次跳转旋转角度的固定大小
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);

//        //设置旋转角度
//        gl.glRotatef(-90,0,1,0);//绕y轴旋转
        gl.glRotatef(roate,0,1,0);//绕y轴旋转
        gl.glRotatef(30,1,0,0);
        roate = roate+rotateStep;
        boolean isOnceDrawAll=false;//是否一次用gl.glDrawArrays画所有的点。false：则调用多次gl.glDrawArrays，每次draw绘制一个点。
        if(isOnceDrawAll){
            //绘制数组
            //得到坐标
            //计算点坐标
            float r = 0.5f;
            List<Float> coordList  = new ArrayList<Float>();
            float x =0,y=0,z=1f;
            float zstep = 0.008f;//点在z轴上一次移动的距离
            for(float alpha=0;alpha<Math.PI*6;alpha=(float)(alpha+Math.PI/32)){
                x = (float)(r*Math.cos(alpha));
                y = (float)(r*Math.sin(alpha));
                z = z-zstep;
                coordList.add(x);
                coordList.add(y);
                coordList.add(z);
            }
            //一次性画所有的点
            //分配字节缓存空间，存放顶点坐标数据
            ByteBuffer ibb = BufferUtil.list2ByteBuffer(coordList);
            //设置绘制颜色
            gl.glColor4f(1,0,0,1);
            //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
            gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
            //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
            gl.glDrawArrays(GL10.GL_POINTS,0,coordList.size()/3);//除以3，是因为3个float表示一个点
        }else{
            //绘制数组
            //得到坐标
            //计算点坐标
            float r = 0.5f;
            float x =0,y=0,z=1f;
            float zstep = 0.008f;//点在z轴上一次移动的距离
            float psize = 1.0f;//点大小
            float pstep = 1f;//每次点之间，size变化大小
            for(float alpha=0;alpha<Math.PI*6;alpha=(float)(alpha+Math.PI/32)){
                x = (float)(r*Math.cos(alpha));
                y = (float)(r*Math.sin(alpha));
                z = z-zstep;
                List<Float> coordList  = new ArrayList<Float>();
                coordList.add(x);
                coordList.add(y);
                coordList.add(z);
                //分配字节缓存空间，存放顶点坐标数据
                ByteBuffer ibb = BufferUtil.list2ByteBuffer(coordList);
                //设置绘制颜色
                gl.glColor4f(1,0,0,1);
                //设置点大小
                gl.glPointSize(psize+(int)(pstep*alpha/Math.PI*6));//不设最默认为1像素大小
                //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
                gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
                //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
                gl.glDrawArrays(GL10.GL_POINTS,0,coordList.size()/3);//除以3，是因为3个float表示一个点
            }
        }
    }
}
