package com.wangshi.MyOpenglTest;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

public class BufferUtil {
    /**
     * 将浮点数组转换为ByteBuffer缓冲区
     */
    public static ByteBuffer arr2ByteBuffer(float[] arr){
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = ByteBuffer.allocateDirect(arr.length*4);
        //设置的顺序(本地顺序)
        ibb.order(ByteOrder.nativeOrder());
        //放置坐标数组
        FloatBuffer fbb = ibb.asFloatBuffer();
        fbb.put(arr);
        //定位指针位置，从该位置开始读取
        ibb.position(0);
        return ibb;
    }
    /**
     * 将浮点数组转换为ByteBuffer缓冲区
     */
    public static ByteBuffer arr2ByteBuffer(byte[] arr){
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = ByteBuffer.allocateDirect(arr.length*4);
        //设置的顺序(本地顺序)
        ibb.order(ByteOrder.nativeOrder());
        //放置坐标数组
        ibb.put(arr);
        //定位指针位置，从该位置开始读取
        ibb.position(0);
        return ibb;
    }
    /**
     * 将浮点列表-list 转化为ByteBuffer缓冲区
     */
    public static ByteBuffer list2ByteBuffer(List<Float> list ){
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = ByteBuffer.allocateDirect(list.size()*4);
        //设置的顺序(本地顺序)
        ibb.order(ByteOrder.nativeOrder());
        //放置坐标数组
        FloatBuffer fbb = ibb.asFloatBuffer();
        for(Float f:list){
            fbb.put(f);
        }
        //定位指针位置，从该位置开始读取
        ibb.position(0);
        return ibb;
    }
    /**
     * 将浮点数组转换为ByteBuffer缓冲区
     */
    public static FloatBuffer arr2FloatBuffer(float[] arr){
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = ByteBuffer.allocateDirect(arr.length*4);
        //设置的顺序(本地顺序)
        ibb.order(ByteOrder.nativeOrder());
        //放置坐标数组
        FloatBuffer fbb = ibb.asFloatBuffer();
        fbb.put(arr);
        //定位指针位置，从该位置开始读取
        fbb.position(0);
        return fbb;
    }
    /**
     * 将浮点列表-list 转化为FloatBuffer缓冲区
     */
    public static FloatBuffer list2FloatBuffer(List<Float> list ){
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = ByteBuffer.allocateDirect(list.size()*4);
        //设置的顺序(本地顺序)
        ibb.order(ByteOrder.nativeOrder());
        //放置坐标数组
        FloatBuffer fbb = ibb.asFloatBuffer();
        for(Float f:list){
            fbb.put(f);
        }
        //定位指针位置，从该位置开始读取
        fbb.position(0);
        return fbb;
    }
    /**
     * 绘制球体
     */
    public static void drawSphere(GL10 gl,float R,int stack,int slice){
        //计算球体坐标
        float stackStep = (float) Math.PI/stack;
        float sliceStep = (float) Math.PI/slice;
        float r0,r1,y0,y1,x0,x1,z0,z1;
        float alpha0=0;
        float alpha1=0;
        float bata = 0;
        ArrayList<Float> coordList = new ArrayList<>();
        for(int i=0;i<stack;i++){
            alpha0 =(float) (-Math.PI/2+i*stackStep);
            alpha1 =(float) (-Math.PI/2+(i+1)*stackStep);
            y0 = (float) (R* Math.sin(alpha0));
            r0 =(float) (R*Math.cos(alpha0));
            y1 = (float) (R*Math.sin(alpha1));
            r1 =(float) (R*Math.cos(alpha1));
            for(int j=0;j<=slice*2;j++){
                bata = j*sliceStep;
                x0=(float) (r0*Math.cos(bata));
                z0=(float) (-r0*Math.sin(bata));
                x1=(float) (r1*Math.cos(bata));
                z1=(float) (-r1*Math.sin(bata));
                coordList.add(x0);
                coordList.add(y0);
                coordList.add(z0);
                coordList.add(x1);
                coordList.add(y1);
                coordList.add(z1);
            }

        }
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = BufferUtil.list2ByteBuffer(coordList);
        //设置绘制颜色
        gl.glColor4f(1,0,0,1);
        //设置点大小
        gl.glPointSize(1);//不设最默认为1像素大小
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
        //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,coordList.size()/3);//除以3，是因为3个float表示一个点
    }
}
