package com.wangshi.MyOpenglTest.example11;

import android.content.Context;
import android.content.res.Resources;
import android.opengl.GLES20;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ShaderUtils {
    private static final String TAG = "ShaderUtils";
    //gl错误检查
    public static void checkGlError(String label) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG, label + ": glError " + error);
            throw new RuntimeException(label + ": glError " + error);
        }
    }
    /**
     * 创建程序
     * 1.创建一个程序对象
     * 2.将编译后的着色器对象连接到程序对象,需要链接顶点着色器和片元着色器
     * 3.链接程序对象
     */

    public static int createProgram(String vertexSource, String fragmentSource) {
        //加载着色器，顶点着色器
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
        if (vertexShader == 0) {
            return 0;
        }
        //加载着色器，片元着色器
        int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
        if (pixelShader == 0) {
            return 0;
        }
        //1.创建着色器程序对象
        int program = GLES20.glCreateProgram();
        if (program != 0) {
            //2.连接着色器 (只能链接到编译成功后的着色器) 断开着色器连接 glDetachShader（）
            GLES20.glAttachShader(program, vertexShader);
//            checkGlError("Attach Vertex Shader");

            //2.连接着色器(只能链接到编译成功后的着色器)
            GLES20.glAttachShader(program, pixelShader);
//            checkGlError("Attach Fragment Shader");

            //3.链接程序对象
            GLES20.glLinkProgram(program);
            int[] linkStatus = new int[1];
            //查询程序对象信息
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {
                Log.e(TAG, "Could not link program: ");
                Log.e(TAG, GLES20.glGetProgramInfoLog(program));
                GLES20.glDeleteProgram(program);//删除程序对象
                program = 0;
            }
        }
        return program;
    }

    /**
     * 加载着色器 分三步
     * 1.创建一个着色器对象
     * 2.将源代码连接到着色器对象
     * 3.编译着色器对象
     * 创建着色器对象（glCreateShader）,删除着色器（glDeleteShader）
     * 着色其器有两种类型
     * @param shaderType 着色其类型
     *  GLES20.GL_VERTEX_SHADER 顶点着色器
     *  GLES20.GL_FRAGMENT_SHADER 片源着色器
     * @param source 着色器代码
     *
     * @return 结果为0加载失败,结果不为0加载成功
     */
    private static int loadShader(int shaderType, String source) {
        //1.创建着色器
        int shader = GLES20.glCreateShader(shaderType);
        if (shader != 0) {
            //2.将源代码连接着色器对象
            GLES20.glShaderSource(shader, source);
            //3.编译着色器对象
            GLES20.glCompileShader(shader);
            int[] compiled = new int[1];
            /**
             * 查询着色器对象信息 通过信息判断是否成功编译着色器对象。
             *shader:着色器对象句柄
             *pname:信息参数
             *GL_COMPILE_STATUS
             *GL_DELETE_STATUS
             *GL_INFO_LOG_LENGTH
             *GL_SHADER_SOURCE_LENGTH
             *GL_SHADER_TYPE
             *params:指向查询结果的整数存储位置的指针
             * void glGetShaderiv(GLuint shader, GLenum pname, GLint *params)
             */
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            if (compiled[0] == 0) {
                Log.e("------------------>","编译失败");
                Log.e(TAG, "Could not compile shader " + shaderType + ":");
                Log.e(TAG, GLES20.glGetShaderInfoLog(shader));
                GLES20.glDeleteShader(shader);//删除着色器
                shader = 0;
            }else{
                Log.e("------------------>","编译成功");
            }

        }
        return shader;
    }

    //从raw资源文件中读取着色器文本
    public static String readRawTextFile(Context context, int resId) {
        InputStream inputStream = context.getResources().openRawResource(resId);
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    //从asset资源文件中读取着色器文本
    public static String loadFromAssetsFile(String fname, Resources res){
        StringBuilder result=new StringBuilder();
        try{
            InputStream is=res.getAssets().open(fname);
            int ch;
            byte[] buffer=new byte[1024];
            while (-1!=(ch=is.read(buffer))){
                result.append(new String(buffer,0,ch));
            }
        }catch (Exception e){
            return null;
        }
        return result.toString().replaceAll("\\r\\n","\n");
    }
}
