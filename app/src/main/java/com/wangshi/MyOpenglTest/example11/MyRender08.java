package com.wangshi.MyOpenglTest.example11;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLES32;
import android.opengl.GLU;
import android.util.Log;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.R;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 *  绘制纹理，着色器和程序
 * 源代码提供给着色器对象，然后着色器对象被编译为一个目标文件，编译后可以连接到一个程序对象。
 * 程序对象可以连接多个着色器对象。在OpenGL ES中，每个程序对象必须连接一个顶点着色器和一个片段着色器。
 * 一般包含6个步骤：
 * 1.创建一个顶点着色器对象和一个片段着色器对象
 * 2.将源代码连接到每个着色器对象
 * 3.编译着色器对象
 * 4.创建一个程序对象
 * 5.将编译后的着色器对象连接到程序对象
 * 6.链接程序对象
 */
public class MyRender08 extends AbstractRender {

    private float roate=0;
    private float rotateStep=0.2f;//每次跳转旋转角度的固定大小
    private Resources resource;
    private Bitmap bitmap0,bitmap,bitmap2,bitmap3;

    private boolean isClear;
    private long createTime;
    private GlBitmapTexture glBitmapTexture,glBitmapTexture2,glBitmapTexture3;
    private boolean isSaveBitmap;//保存纹理到文件

    //着色器相关
    private int programId = -1;//程序id
    //顶点位置坐标
    private int aPositionHandle;
    //纹理位置坐标
    private int aTextureCoordHandle;
    //程序传入2d纹理
    private int uTextureSamplerHandle;
    //程序传入 smooth
    private int smoothHandle;
    private int[] bos = new int[2];
//    GLBitmapRenderer glBitmapRenderer;
    public MyRender08(Resources resource){
        this.resource = resource;
        bitmap0 = BitmapFactory.decodeResource(resource, R.mipmap.test01);
        bitmap = BitmapFactory.decodeResource(resource, R.mipmap.test03);
        bitmap2 = BitmapFactory.decodeResource(resource, R.mipmap.test04);
        bitmap3 = BitmapFactory.decodeResource(resource, R.mipmap.test06);
        createTime = System.currentTimeMillis();
    }
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
       super.onSurfaceCreated(gl,config);
        glBitmapTexture = new GlBitmapTexture(gl,bitmap,0,0,1.0f,1.0f);
        glBitmapTexture2 = new GlBitmapTexture(gl,bitmap2,0,0,1.0f,1.0f);
        glBitmapTexture3 = new GlBitmapTexture(gl,bitmap3,0,0,1.0f,1.0f);
//        initShader();

    }
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl,width,height);
        glBitmapTexture.setRatio(ratio);
        glBitmapTexture2.setRatio(ratio);
        glBitmapTexture3.setRatio(ratio);
//        glBitmapRenderer = new GLBitmapRenderer();
//        glBitmapRenderer.initShader(bitmap0);
    }
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)

        gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,3,0,0,0,0,1,0);


        if(System.currentTimeMillis()-createTime>=5000){
            if(!isClear){
                isClear = true;
                //删除纹理 防止内存溢出 参数：n要删除的纹理个数，textures纹理数组，offset 偏移（默认设为0就行了）
                gl.glDeleteTextures(1,glBitmapTexture3.getTexureId(),0);
                gl.glDeleteTextures(1,glBitmapTexture2.getTexureId(),0);
//                gl.glFlush();
            }
        }
        if(!isClear){
            //绘制
            //保持图片宽高比例
            glBitmapTexture3.draw(gl,0.6f,1.6f,1.0f,1.0f* glBitmapTexture3.getBitmapRatio());
            glBitmapTexture2.draw(gl,0.5f,0.8f,1.0f,1.0f*glBitmapTexture2.getBitmapRatio());
            glBitmapTexture.draw(gl,0.2f,0.3f,1.0f,1.0f*glBitmapTexture.getBitmapRatio());
//            if(!isSaveBitmap){
//                isSaveBitmap = true;
//                String path = Environment.getExternalStorageDirectory()+"test.png";
//                saveTextureToImage(glBitmapTexture.getTexureId()[0],500,5000,path );
//            }
        }else{
            glBitmapTexture.draw(gl,0.2f,0.3f,1.0f,1.0f*glBitmapTexture.getBitmapRatio());
        }
//        drawFrame(glBitmapTexture.getTexureId()[0]);
//        glBitmapRenderer.drawFrame();
    }
    private void saveTextureToImage(int textureID, int width, int height, String fileName) {
        int[] frameBuffer = new int[1];
        GLES32.glGenFramebuffers(1, frameBuffer, 0);
        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, frameBuffer[0]);
        GLES32.glFramebufferTexture2D(GLES32.GL_FRAMEBUFFER, GLES32.GL_COLOR_ATTACHMENT0, GLES32.GL_TEXTURE_2D, textureID, 0);
        GLES32.glDrawBuffers(1, new int[]{GLES32.GL_COLOR_ATTACHMENT0}, 0);
        if (GLES32.glCheckFramebufferStatus(GLES32.GL_FRAMEBUFFER) != GLES32.GL_FRAMEBUFFER_COMPLETE) {
            Log.e("---------------->", "framebuffer not complete");
            return;
        }
        ByteBuffer rgbaBuf = ByteBuffer.allocateDirect(width * height * 4);
        rgbaBuf.position(0);
        GLES32.glReadPixels(0, 0, width, height, GLES32.GL_RGBA, GLES32.GL_UNSIGNED_BYTE, rgbaBuf);
        saveRgb2Bitmap(rgbaBuf, fileName, width, height);
        GLES32.glDeleteFramebuffers(1, IntBuffer.wrap(frameBuffer));
        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
    }
    private void saveRgb2Bitmap(Buffer buf, String filename, int width, int height) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bmp.copyPixelsFromBuffer(buf);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, bos);
            bmp.recycle();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * 精度限定符：
     * lowp，meidump，highp
     * highp vec4 position;
     * varying lowp vec4 color;
     * mediump float specularExp;
     *
     * 指定默认精度
     * presision highp float;
     * presision medium int;
     *
     * 数据类型：
     * 1. float  bool  int    基本数据类型
     * 2. vec2                包含了2个浮点数的向量
     * 3. vec3                包含了3个浮点数的向量
     * 4. vec4                包含了4个浮点数的向量
     * 5. ivec2               包含了2个整数的向量
     * 6. ivec3               包含了3个整数的向量
     * 7. ivec4               包含了4个整数的向量
     * 8. bvec2               包含了2个布尔数的向量
     * 9. bvec3               包含了3个布尔数的向量
     * 10. bvec4               包含了4个布尔数的向量
     * 11. mat2                2*2维矩阵
     * 12. mat3                3*3维矩阵
     * 13. mat4                4*4维矩阵
     * 14. sampler1D           1D纹理采样器
     * 15. sampler2D           2D纹理采样器
     * 16. sampler3D           3D纹理采样器
     * precision mediump float;                           // 设置工作精度
     * varying vec4 vColor;                               // 接收从顶点着色器过来的顶点颜色数据
     * varying vec2 vTexCoord;                           // 接收从顶点着色器过来的纹理坐标
     * uniform sampler2D sTexture;                        // 纹理采样器，代表一幅纹理
     * uniform 程序传入的值,这里传入的值可以在片元着色器中使用
     * 三种向OpenGL着色器传递数据的方法
     * 1.attribute:是只能在顶点着色器中使用的变量,一般用来表示一些顶点的数据，attribute不能直接传给片元着色器。
     * 2.uniform：uniform变量是外部application程序传递给着色器的变量。
     * ，着色器只能用而不能修改uniform变量
     * 3.varying：从顶点着色器传递到片元着色器的量，如用于传递到片元着色器中的顶点颜色，可以使用varying（易变变量）。
     *
     * 变量定义
     * 举例
     * vec4 myVec4 = vec4(1.0)	vec4 myVec4 = vec4{1.0, 1.0,1.0,1.0}
     * vec3 myVec3 = vec3(1.0,0.0,0.5)	myVec3 = {1.0, 0.0, 0.5}
     * vec3 temp = vec3(myVec3)	temp = myVec3
     * //矩阵的每个分量可以用{r, g, b, a} {x, y, z, w} {s, t, p, q}
     * vec2 myVec2 = vec2(myVec3)	myVec2 = {myVec3.x, myVec3.y}
     * myVec4 = vec4(myVec2, temp)	myVec4 {myVec2.x, myVec2.y, temp.x, temp.y}
     * 分量顺序还可以被混合（Swizzling）重组
     * vec4 pos = vec4(1.0, 2.0, 3.0, 4.0);
     * vec4 swiz= pos.wzyx; // swiz = (4.0, 3.0, 2.0, 1.0)
     * vec4 dup = pos.xxyy; // dup = (1.0, 1.0, 2.0, 2.0)
     * 在着色器脚本中使用 #version 300 es 声明为指定使用 OpenGL ES 3.0 版本
     * 而不添加版本声明或者使用 #version 100 es 声明版本则指定使用 OpenGL ES 2.0
     */
    public void initShader() {
        //片元着色器，绿幕抠图
        String fragmentShader =
                //变换后的纹理坐标
                "varying highp vec2 vTexCoord;\n" +
                //程序传入的图片纹理
                "uniform sampler2D sTexture;\n" +
                //程序传入的值
                "uniform highp float smooth;\n" +
                "highp vec3 rgb2hsv(highp vec3 c){\n" +
                "    highp vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);\n" +
                "    highp vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));\n" +
                "    highp vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));\n" +
                "    highp float d = q.x - min(q.w, q.y);\n" +
                "    highp float e = 1.0e-10;\n" +
                "    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);\n" +
                "}\n"+
//                "void modifyColor(vec4 color){\n" +
//                "    color.r=max(min(color.r,1.0),0.0);\n" +
//                "    color.g=max(min(color.g,1.0),0.0);\n" +
//                "    color.b=max(min(color.b,1.0),0.0);\n" +
//                "    color.a=max(min(color.a,1.0),0.0);\n" +
//                "}\n"+
                "void main() {\n" +
                "   highp vec4 rgba = texture2D(sTexture , vec2(vTexCoord.x,1.0 - vTexCoord.y));\n" +
                "   highp float rbAverage = (rgba.r + rgba.b)*0.8;\n"+
                "   highp vec3 hsv = rgb2hsv(rgba.rgb);\n"+
                "   highp float hmin = 0.19444000;\n" +
                "   highp float hmax = 0.42777888;\n" +
                "   highp float smin = 0.16862000;\n" +
                "   highp float smax = 1.0;\n" +
                "   highp float vmin = 0.18039000;\n" +
                "   highp float vmax = 1.0;\n" +
                "   int gs = 0;\n"+
                "   if(hsv.x >= hmin && hsv.x <= hmax &&\n" +
                "       hsv.y >= smin && hsv.y <= smax &&\n" +
                "       hsv.z >= vmin && hsv.z <= vmax){\n" +
                "       gs = 1;\n"+
                "   }else if(rgba.g >= rbAverage && rgba.g > 0.6){\n" +
                "       gs = 1;\n"+
                "   }\n"+
                "   if(gs == 1){\n" +
                "       rbAverage = (rgba.r + rgba.b)*0.65;\n"+
                "       if(rbAverage > rgba.g)rbAverage = rgba.g;"+
                "       highp float gDelta = rgba.g - rbAverage;\n"+
                "       highp float ss = smoothstep(0.0, smooth, gDelta);\n"+
                "       rgba.a = 1.0 - ss;\n"+
                "       rgba.a = rgba.a * rgba.a * rgba.a;\n"+
                "       rgba = mix(vec4(0.0),rgba,rgba.a);\n"+
//                "       rgba.a = 0.0;\n"+     //这样可以大幅度减少绿边,但是会丢失细节
                "   }\n"+
//                "   highp vec4 deltaColor = rgba + vec4(0.1, 0.1, 0.0, 0.0);\n"+
//                "   modifyColor(deltaColor);\n"+
//                "   gl_FragColor = deltaColor;\n" +
//                "   lowp float average = (rgba.r + rgba.g + rgba.b) / 3.0;\n" +
//                "   lowp float mx = max(rgba.r, max(rgba.g, rgba.b));\n" +
//                "   lowp float amt = (mx - average) * (0.1 * 3.0);\n" +
//                "   rgba.rgb = mix(rgba.rgb, vec3(mx), amt);\n" +
                "   gl_FragColor = rgba;\n"+
                "}";
        //标准的的顶点着色器.
        String vertexShader =
                //顶点位置坐标
                "attribute vec4 aPosition;\n" +
                //纹理位置坐标
                "attribute vec2 aTexCoord;\n" +
                //纹理坐标 用于接收和传递给片元着色器的纹理坐标  name必需与片元着色器中的一致
                "varying vec2 vTexCoord;\n" +
                "void main() {\n" +
                "  vTexCoord = aTexCoord;\n" +
                "  gl_Position = aPosition;\n" +
                "}";
        programId = ShaderUtils.createProgram(vertexShader, fragmentShader);
        //顶点位置坐标, name要和顶点着色器里的一致
        aPositionHandle = GLES20.glGetAttribLocation(programId, "aPosition");
        //程序传入的图片纹理, name要和片元着色器里的一致
        uTextureSamplerHandle = GLES20.glGetUniformLocation(programId, "sTexture");
        //纹理位置坐标, name要和顶点着色器里的一致
        aTextureCoordHandle = GLES20.glGetAttribLocation(programId, "aTexCoord");
        //程序传入的值, name要和片元着色器里的一致
        smoothHandle = GLES20.glGetUniformLocation(programId, "smooth");

        //顶点坐标
        float[] vertexData = {
                1f, -1f, 0f,
                -1f, -1f, 0f,
                1f, 1f, 0f,
                -1f, 1f, 0f
        };

        //纹理坐标
        float[] textureVertexData = {
                1f, 0f,
                0f, 0f,
                1f, 1f,
                0f, 1f
        };
        FloatBuffer vertexBuffer = ByteBuffer.allocateDirect(vertexData.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(vertexData);
        vertexBuffer.position(0);

        FloatBuffer textureVertexBuffer = ByteBuffer.allocateDirect(textureVertexData.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
                .put(textureVertexData);
        textureVertexBuffer.position(0);
        GLES20.glGenBuffers(2, bos, 0);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, bos[0]);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vertexData.length * 4, vertexBuffer, GLES20.GL_STATIC_DRAW);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, bos[1]);
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, textureVertexData.length * 4, textureVertexBuffer, GLES20.GL_STATIC_DRAW);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
    }

    //释放程序
    public void release() {
        GLES20.glDeleteProgram(programId);
        GLES20.glDeleteBuffers(bos.length, bos, 0);
    }
    public void drawFrame(int texture) {
        GLES20.glUseProgram(programId);
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, texture);
        GLES20.glUniform1i(uTextureSamplerHandle, 0);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, bos[0]);
        GLES20.glEnableVertexAttribArray(aPositionHandle);
        GLES20.glVertexAttribPointer(aPositionHandle, 3, GLES20.GL_FLOAT, false,
                0, 0);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, bos[1]);
        GLES20.glEnableVertexAttribArray(aTextureCoordHandle);
        GLES20.glVertexAttribPointer(aTextureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, 0);
        GLES20.glUniform1f(smoothHandle, 0.2f);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
    }
}
