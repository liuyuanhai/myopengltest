package com.wangshi.MyOpenglTest.example11;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wangshi.MyOpenglTest.MyGLSurfaceview;

/**
 * 绘制纹理，着色器和程序
 */
public class Activity08 extends AppCompatActivity {

    private MyGLSurfaceview glSurfaceView;
//    private MyRender08 render;
    private MyRender08 render;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glSurfaceView = new MyGLSurfaceview(this);
        //设置颜色缓存为RGB_565 ,深度缓存位数为最少16,模板缓存位数最少为4
//        glSurfaceView.setEGLConfigChooser(5,6,5,1,16,8);
        //设置渲染器
        render = new MyRender08(getResources());
        render.setIs_GL_COLOR_ARRAY(false);
        //当使用到着色器时（实现滤镜），需要指定客户端opengles的版本，否则将无法生效导致bug.
        glSurfaceView.setEGLContextClientVersion(2);
        glSurfaceView.setRenderer(render);

        /**
         *  设置渲染模式
         *  1.GLSurfaceView.RENDERMODE_CONTINUOUSLY 持续渲染渲染（默认）
         *  2.GLSurfaceView.RENDERMODE_WHEN_DIRTY 脏渲染,命令渲染（请求时渲染）
         *    使用glSurfaceView.requestRender();发出渲染命令
         */
        setContentView(glSurfaceView);
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        render.release();
    }
}
