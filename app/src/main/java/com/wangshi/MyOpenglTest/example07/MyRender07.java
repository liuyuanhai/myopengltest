package com.wangshi.MyOpenglTest.example07;

import android.opengl.GLU;
import android.util.FloatMath;

import androidx.appcompat.widget.ResourceManagerInternal;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

/**
 *  绘制立体圆环
 */
public class MyRender07 extends AbstractRender {

    private float roate=0;
    private float rotateStep=0.2f;//每次跳转旋转角度的固定大小
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)
        gl.glShadeModel(GL10.GL_FLAT);
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);
        gl.glRotatef(roate,0,1,0);//绕y轴旋转
        gl.glRotatef(30,1,0,0);
        roate = roate+rotateStep;

        float Rinner = 0.2f;
        float Ring = 0.15f;
        int count = 40;
        float alphaStep = (float)(Math.PI*2/count);
        float alpha = 0;
        float x0,y0,z0,x1,y1,z1;
        int count0 =40;
        float betaStep = (float)(Math.PI*2/count0);
        float beta = 0;
        ArrayList<Float> coordList = new ArrayList<>();
        for(int i=0;i<count;i++){
            alpha = i*alphaStep;
            for(int j=0;j<count0;j++){
                beta = j*betaStep;
                x0  = (float)(Math.cos(alpha)*(Rinner+Ring*(1+Math.cos(beta))));
                y0  = (float)(Math.sin(alpha)*(Rinner+Ring*(1+Math.cos(beta))));
                z0  = (float)(-Ring*Math.sin(beta));
                x1  = (float)(Math.cos(alpha+alphaStep)*(Rinner+Ring*(1+Math.cos(beta))));
                y1  = (float)(Math.sin(alpha+alphaStep)*(Rinner+Ring*(1+Math.cos(beta))));
                z1  = (float)(-Ring*Math.sin(beta));
                coordList.add(x0);
                coordList.add(y0);
                coordList.add(z0);
                coordList.add(x1);
                coordList.add(y1);
                coordList.add(z1);
            }
        }
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = BufferUtil.list2ByteBuffer(coordList);
        //设置绘制颜色
        gl.glColor4f(1,0,0,1);
        //设置点大小
        gl.glPointSize(1);//不设最默认为1像素大小
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
        //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
        gl.glDrawArrays(GL10.GL_LINE_STRIP,0,coordList.size()/3);//除以3，是因为3个float表示一个点
    }
}
