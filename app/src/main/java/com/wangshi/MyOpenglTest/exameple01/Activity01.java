package com.wangshi.MyOpenglTest.exameple01;



import android.opengl.GLSurfaceView;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wangshi.MyOpenglTest.MyGLRenderer;
import com.wangshi.MyOpenglTest.MyGLSurfaceview;
import com.wangshi.MyOpenglTest.R;

/**
 * 简单绘制一个三角形
 */
public class Activity01 extends AppCompatActivity {

    private MyGLSurfaceview glSurfaceView;
    private MyRender01 myRender01;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glSurfaceView = new MyGLSurfaceview(this);

        myRender01 = new MyRender01();
        myRender01.setIs_GL_COLOR_ARRAY(false);
        //设置渲染器
        glSurfaceView.setRenderer(myRender01);
        /**
         *  设置渲染模式
         *  1.GLSurfaceView.RENDERMODE_CONTINUOUSLY 持续渲染渲染（默认）
         *  2.GLSurfaceView.RENDERMODE_WHEN_DIRTY 脏渲染,命令渲染（请求时渲染）
         *    使用glSurfaceView.requestRender();发出渲染命令
         */
        glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        setContentView(glSurfaceView);
    }
}
