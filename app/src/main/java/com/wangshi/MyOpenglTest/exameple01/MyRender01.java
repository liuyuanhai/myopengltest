package com.wangshi.MyOpenglTest.exameple01;

import android.opengl.GLSurfaceView;
import android.opengl.GLU;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyRender01 extends AbstractRender {
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);
        //画三角形
        //绘制数组
        //得到坐标
        float[] coords = new float[]{
            0f,ratio,2f,
            -1f,-ratio,2f,
            1,-ratio,2f
        };
        //分配字节缓存空间，存放顶点坐标数据
        ByteBuffer ibb = BufferUtil.arr2ByteBuffer(coords);
        //设置绘制颜色
        gl.glColor4f(1,0,0,1);
        //设置顶点指针 参数：3表示三维点，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,ibb);
        //GL10.GL_TRIANGLES 绘制三角形,first 起始位置，count 点的数量
        gl.glDrawArrays(GL10.GL_TRIANGLES,0,3);
    }
}
