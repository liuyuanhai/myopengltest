package com.wangshi.MyOpenglTest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class GLBitmapCut {
    private FloatBuffer textureBuffer; // buffer holding the texture coordinates
    //标准纹理坐标 在opengl中，图片：宽为1.0 图片高为1.0
    private float texture[] = {
            // Mapping coordinates for the vertices
            0.0f, 1.0f, // top left (V2)
            0.0f, 0.0f, // bottom left (V1)
            1.0f,1.0f, // top right (V4)
            1.0f, 0.0f // bottom right (V3)
    };
    //标准顶点坐标
    private FloatBuffer vertexBuffer; // buffer holding the vertices din
    private float vertices[] = { -1.0f, -1.0f, 0.0f, // V1 - bottom left
            -1.0f, 1.0f, 0.0f, // V2 - top left
            1.0f, -1.0f, 0.0f, // V3 - bottom right
            1.0f, 1.0f, 0.0f // V4 - top right
    };
    private int bitmapWidth;
    private int bitmapHeight;
    public GLBitmapCut(Context context,float cutStartX,float cutStartY,float cutEndX,float cutEndY,float locationX,float locationY) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.test);
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        bitmap.recycle();
        bitmap = null;
        reset(bitmapWidth,bitmapHeight,cutStartX,cutStartY,cutEndX,cutEndY,locationX,locationY);//设置图片显示位置
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        vertexBuffer = byteBuffer.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.position(0);
        byteBuffer = ByteBuffer.allocateDirect(texture.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        textureBuffer = byteBuffer.asFloatBuffer();
        textureBuffer.put(texture);
        textureBuffer.position(0);
    }

    /** The texture pointer */

    private int[] textures = new int[1];//1表示需要生成一个纹理
    public void loadGLTexture(GL10 gl, Context context) {
        // loading texture
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.test);
        bitmapWidth = bitmap.getWidth();
        bitmapHeight = bitmap.getHeight();
        Log.e("------------->","bitmapWidth="+bitmapWidth+";bitmapHeight="+bitmapHeight);
        // generate one texture pointer
        gl.glGenTextures(1, textures, 0);
        // ...and bind it to our array
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
        // create nearest filtered texture
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_NEAREST);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER,GL10.GL_LINEAR);
        // Use Android GLUtils to specify a two-dimensional texture image from
        // our bitmap
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
        // Clean up
        bitmap.recycle();
    }

    public void draw(GL10 gl) {
        // bind the previously generated texture
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
        // Point to our buffers
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        // Set the face rotation
        gl.glFrontFace(GL10.GL_CW);
        // Point to our vertex buffer
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
        // Draw the vertices as triangle strip
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);
        // Disable the client state before leaving
        gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
    }
    //对纹理进行裁剪
    private float[] getCutTexture(int startX,int startY,int endX,int endY){
        float startXFloat = 1.0f*startX/bitmapWidth;
        float startYFloat = 1.0f*startY/bitmapHeight;
        float endXFloat = 1.0f*endX/bitmapWidth;
        float endYFloat = 1.0f*endY/bitmapHeight;
        float texture[] = {
                // Mapping coordinates for the vertices
                startXFloat, endYFloat, // top left (V2)
                startXFloat, startYFloat, // bottom left (V1)
                endXFloat,endYFloat, // top right (V4)
                endXFloat, startYFloat // bottom right (V3)
        };
        return texture;
    }
    //重设纹理坐标和顶点坐标
    //取值范围0-1 float
    private void reset(int bitmapWidth,int bitmapHeight,float cutStartX,float cutStartY,float cutEndX,float cutEndY,float locationX,float locationY){
        texture = new float[]{
                // Mapping coordinates for the vertices
                cutStartX, cutEndY, // top left (V2)
                cutStartX, cutStartY, // bottom left (V1)
                cutEndX,cutEndY, // top right (V4)
                cutEndX, cutStartY // bottom right (V3)
        };
        float width = cutEndX-cutStartX;
        float height = cutEndY-cutStartY;
        float heightScale = bitmapHeight*1.0f/bitmapWidth;
        vertices= new float[]{ -1.0f+locationX*2, heightScale*(1.0f-(locationY*2+height*2)), 0.0f, // V1 - bottom left
                -1.0f+locationX*2, heightScale*(1.0f-locationY*2), 0.0f, // V2 - top left
                -1.0f+locationX*2+width*2,heightScale*(1.0f-(locationY*2+height*2)), 0.0f, // V3 - bottom right
                -1.0f+locationX*2+width*2,heightScale*(1.0f-locationY*2), 0.0f, 0.0f // V4 - top right
        };
    }
}
