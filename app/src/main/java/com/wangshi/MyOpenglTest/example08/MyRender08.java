package com.wangshi.MyOpenglTest.example08;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES32;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.os.Environment;
import android.util.Log;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;
import com.wangshi.MyOpenglTest.R;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;

/**
 *  绘制纹理
 */
public class MyRender08 extends AbstractRender {

    private float roate=0;
    private float rotateStep=0.2f;//每次跳转旋转角度的固定大小
    private Resources resource;
    private Bitmap bitmap,bitmap2,bitmap3;

    private boolean isClear;
    private long createTime;
    private GlBitmapTexture glBitmapTexture,glBitmapTexture2,glBitmapTexture3;
    private boolean isSaveBitmap;//保存纹理到文件
    private Context context;
    public MyRender08(Resources resource,Context context){
        this.resource = resource;
        Bitmap bitmapTemp = BitmapFactory.decodeResource(resource, R.mipmap.test03);
        //单张纹理添加一个滤镜
        GPUImage gpuImage = new GPUImage(context);
        gpuImage.setImage(bitmapTemp);
        // 应用黑白滤镜
        gpuImage.setFilter(new GPUImageGrayscaleFilter());
        bitmap = gpuImage.getBitmapWithFilterApplied();
        bitmapTemp.recycle();
        bitmap2 = BitmapFactory.decodeResource(resource, R.mipmap.test04);
        bitmap3 = BitmapFactory.decodeResource(resource, R.mipmap.test06);
        createTime = System.currentTimeMillis();
    }
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
       super.onSurfaceCreated(gl,config);
        glBitmapTexture = new GlBitmapTexture(gl,bitmap,0,0,1.0f,1.0f);
        glBitmapTexture2 = new GlBitmapTexture(gl,bitmap2,0,0,1.0f,1.0f);
        glBitmapTexture3 = new GlBitmapTexture(gl,bitmap3,0,0,1.0f,1.0f);

    }
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl,width,height);
        glBitmapTexture.setRatio(ratio);
        glBitmapTexture2.setRatio(ratio);
        glBitmapTexture3.setRatio(ratio);
    }
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)
        gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,3,0,0,0,0,1,0);


        if(System.currentTimeMillis()-createTime>=5000){
            if(!isClear){
                isClear = true;
                //删除纹理 防止内存溢出 参数：n要删除的纹理个数，textures纹理数组，offset 便宜（默认设为0就行了）
                gl.glDeleteTextures(1,glBitmapTexture3.getTexureId(),0);
                gl.glDeleteTextures(1,glBitmapTexture2.getTexureId(),0);
//                gl.glFlush();
            }
        }
        if(!isClear){
            //绘制
            //保持图片宽高比例
            glBitmapTexture3.draw(gl,0.6f,1.6f,1.0f,1.0f* glBitmapTexture3.getBitmapRatio());
            glBitmapTexture2.draw(gl,0.5f,0.8f,1.0f,1.0f*glBitmapTexture2.getBitmapRatio());
            glBitmapTexture.draw(gl,0.2f,0.3f,1.0f,1.0f*glBitmapTexture.getBitmapRatio());
//            if(!isSaveBitmap){
//                isSaveBitmap = true;
//                String path = Environment.getExternalStorageDirectory()+"test.png";
//                saveTextureToImage(glBitmapTexture.getTexureId()[0],500,5000,path );
//            }
        }else{
            glBitmapTexture.draw(gl,0.2f,0.3f,1.0f,1.0f*glBitmapTexture.getBitmapRatio());
        }


    }
    private void saveTextureToImage(int textureID, int width, int height, String fileName) {
        int[] frameBuffer = new int[1];
        GLES32.glGenFramebuffers(1, frameBuffer, 0);
        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, frameBuffer[0]);
        GLES32.glFramebufferTexture2D(GLES32.GL_FRAMEBUFFER, GLES32.GL_COLOR_ATTACHMENT0, GLES32.GL_TEXTURE_2D, textureID, 0);
        GLES32.glDrawBuffers(1, new int[]{GLES32.GL_COLOR_ATTACHMENT0}, 0);
        if (GLES32.glCheckFramebufferStatus(GLES32.GL_FRAMEBUFFER) != GLES32.GL_FRAMEBUFFER_COMPLETE) {
            Log.e("---------------->", "framebuffer not complete");
            return;
        }
        ByteBuffer rgbaBuf = ByteBuffer.allocateDirect(width * height * 4);
        rgbaBuf.position(0);
        GLES32.glReadPixels(0, 0, width, height, GLES32.GL_RGBA, GLES32.GL_UNSIGNED_BYTE, rgbaBuf);
        saveRgb2Bitmap(rgbaBuf, fileName, width, height);
        GLES32.glDeleteFramebuffers(1, IntBuffer.wrap(frameBuffer));
        GLES32.glBindFramebuffer(GLES32.GL_FRAMEBUFFER, 0);
    }
    private void saveRgb2Bitmap(Buffer buf, String filename, int width, int height) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(filename));
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bmp.copyPixelsFromBuffer(buf);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, bos);
            bmp.recycle();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
