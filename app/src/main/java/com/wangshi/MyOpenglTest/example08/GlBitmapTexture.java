package com.wangshi.MyOpenglTest.example08;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.util.Log;

import com.wangshi.MyOpenglTest.BufferUtil;
import com.wangshi.MyOpenglTest.R;

import javax.microedition.khronos.opengles.GL10;

public class GlBitmapTexture {
    private Bitmap bitmap;
    private float[] textureCoords;//纹理坐标
    private float[] vertexCoords=null;//顶点坐标
    private float bitmapRatio;//图片高比宽的比例
    private int[] texureId = new int[1];//纹理id
    private float ratio;//视口高比宽，比例
    /**
     * 图片纹理的坐标，一张完整的图片，宽高都是从0到1.0
     * 图片可以裁剪中间内容取出
     * @param bitmap 图片bitmap
     * @param xstart   x轴，图片开始位置
     * @param ystart  y轴，图片开始位置
     * @param xend  x轴,图片结束位置
     * @param yend y轴，图片结束位置
     */
    public GlBitmapTexture(GL10 gl,Bitmap bitmap, float xstart, float ystart, float xend, float yend){
        this.bitmap = bitmap;
        textureCoords= new float[]{
                xstart,yend,
                xend,yend,
                xstart,ystart,
                xend,ystart
        };
        loadGLTexture(gl,bitmap);
    }

    public void setRatio(float ratio) {
        this.ratio = ratio;
    }

    public float getBitmapRatio() {
        return bitmapRatio;
    }

    public void loadGLTexture(GL10 gl, Bitmap bitmap){
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();
        GLES20.glGenTextures(1,texureId,0);//生成纹理ID 一个纹理只生成一个id,不要重复生成，否则内存快速溢出导致崩溃
        GLES20.glBindTexture(GL10.GL_TEXTURE_2D,texureId[0]);//绑定纹理
        //设置纹理过滤器 图像拉大/缩小 使用线性插值（相当于直接将图片拉伸到指定大小）
        GLES20.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_MAG_FILTER,GL10.GL_LINEAR);
        GLES20.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_LINEAR);
        //设置纹理回绕参数GL10.GL_TEXTURE_2D 2d纹理；
        // GL10.GL_TEXTURE_WRAP_S,GL10.GL_TEXTURE_WRAP_T，这里的_S/_T表示纹理在横向/竖向上的变化
        //open gl es 只支持GL10.GL_REPEAT和GL10.GL_CLAMP_TO_EDGE。
        //只支持GL10.GL_REPEAT 当纹理的坐标在横向或纵向上大于1.0时，重复渲染，小于时，渲染一部分。
        //GL10.GL_CLAMP_TO_EDGE 当纹理的坐标在横向或纵向上大于1.0时，结束位置颜色延伸
        GLES20.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_WRAP_S,GL10.GL_REPEAT);
        GLES20.glTexParameterf(GL10.GL_TEXTURE_2D,GL10.GL_TEXTURE_WRAP_T,GL10.GL_REPEAT);
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D,0,bitmap,0);//加载纹理
        //图像已经绑定到纹理，可以回收，以免占用内存
        bitmapRatio = (float)bitmapHeight/(float)bitmapWidth;
        bitmap.recycle();
        bitmap = null;
    }

    //三位坐标视口宽度为2，视口高度为2*ratio
    public void draw(GL10 gl,float left,float top,float w,float h) {
        vertexCoords= new float[]{
                -1+left,ratio-top-h,0,
                -1+w+left,ratio-top-h,0,
                -1+left,ratio-top,0,
                -1+w+left,ratio-top,0
        };
        GLES20.glBindTexture(GL10.GL_TEXTURE_2D,texureId[0]);//绑定纹理
        //设置顶点坐标
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, BufferUtil.arr2FloatBuffer(vertexCoords));
        //设置纹理坐标
        gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, BufferUtil.arr2FloatBuffer(textureCoords));
        //将顶点绘制为三角形
        GLES20.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertexCoords.length/3);
    }

    public int[] getTexureId() {
        return texureId;
    }
}
