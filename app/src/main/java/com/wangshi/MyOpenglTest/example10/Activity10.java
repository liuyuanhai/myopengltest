package com.wangshi.MyOpenglTest.example10;


import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wangshi.MyOpenglTest.MyGLSurfaceview;

/**
 * 光照
 */
public class Activity10 extends AppCompatActivity {

    private MyGLSurfaceview glSurfaceView;
    private MyRender10 render;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glSurfaceView = new MyGLSurfaceview(this);
        //设置颜色缓存为RGB_565 ,深度缓存位数为最少16,模板缓存位数最少为4
//        glSurfaceView.setEGLConfigChooser(5,6,5,1,16,8);
        //设置渲染器
        render = new MyRender10(getResources());
        render.setIs_GL_COLOR_ARRAY(false);
        glSurfaceView.setRenderer(render);

        /**
         *  设置渲染模式
         *  1.GLSurfaceView.RENDERMODE_CONTINUOUSLY 持续渲染渲染（默认）
         *  2.GLSurfaceView.RENDERMODE_WHEN_DIRTY 脏渲染,命令渲染（请求时渲染）
         *    使用glSurfaceView.requestRender();发出渲染命令
         */
        setContentView(glSurfaceView);
    }
}
