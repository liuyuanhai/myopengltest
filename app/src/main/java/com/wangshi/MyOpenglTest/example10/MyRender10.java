package com.wangshi.MyOpenglTest.example10;

import android.content.res.Resources;
import android.opengl.GLU;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import javax.microedition.khronos.opengles.GL10;

/**
 * 光照
 *  1.环境光 不具有任何特定的方向 物体表面均匀照亮 ambient
 *  2.散射光 有方向性，类似台灯，窗户外照进的日光。diffuse
 *  3.镜面光 具有很强的方向性specular
 */
public class MyRender10 extends AbstractRender {

    private float roate=0;
    private float rotateStep=0.2f;//每次跳转旋转角度的固定大小
    public MyRender10(Resources resource){
    }
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)
        gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);
        gl.glRotatef(roate,0,1,0);//绕y轴旋转
        gl.glRotatef(30,1,0,0);
        roate = roate+rotateStep;
        //启用光照 启用后，如果没有给光源，则会是一片黑色。
        gl.glEnable(GL10.GL_LIGHTING);//关闭光照gl.glDisable(GL10.GL_LIGHTING);
        //设置全局环境光 全局环境光的默认值为（0.2f,0.2f,0.2f,1f），非常的暗淡；float数值分别为:红/绿/蓝/透明度
        float[] ambientLight = new float[]{1.0f,0.0f,0.0f,1.0f};
        gl.glLightModelfv(GL10.GL_LIGHT_MODEL_AMBIENT,BufferUtil.arr2FloatBuffer(ambientLight));
        //设置材料反射率(环境光和散射光)
        float[] material_ambient_diffuse = new float[]{
                1f,1f,1.0f,1.0f
        };
        gl.glLightfv(GL10.GL_FRONT_AND_BACK,GL10.GL_AMBIENT_AND_DIFFUSE,BufferUtil.arr2FloatBuffer(material_ambient_diffuse));
        //颜色追踪（设置材料的反射率）
        //启用颜色追踪
        gl.glEnable(GL10.GL_COLOR_MATERIAL);
        //光源 0-位置
        float[] lightpos = new float[]{//光源x,y,z 坐标
            0,20,0f
        };
        gl.glEnable(GL10.GL_LIGHT0);
        gl.glLightfv(GL10.GL_LIGHT0,GL10.GL_POSITION,BufferUtil.arr2FloatBuffer(lightpos));
        gl.glColor4f(1,0,0,1);
        BufferUtil.drawSphere(gl,0.5f,8,8);
    }
}
