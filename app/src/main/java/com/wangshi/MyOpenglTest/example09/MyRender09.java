package com.wangshi.MyOpenglTest.example09;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLU;
import android.opengl.GLUtils;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;
import com.wangshi.MyOpenglTest.R;

import javax.microedition.khronos.opengles.GL10;

/**
 *  立方体
 */
public class MyRender09 extends AbstractRender {

    private float roate=0;
    private float rotateStep=0.2f;//每次跳转旋转角度的固定大小
    public MyRender09(Resources resource){
    }
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)
        gl.glShadeModel(GL10.GL_SMOOTH); // Enable Smooth Shading
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);
        gl.glRotatef(roate,0,1,0);//绕y轴旋转
        gl.glRotatef(30,1,0,0);
        roate = roate+rotateStep;
        //定义点坐标
        float r=0.4f;
        float[] coords = new float[]{//下面的正方形的点逆时针为：0，1，3，2。上面的正方形的点逆时针为：4，5，7，6
            -r,r,r, //前面的点 左上
            -r,-r,r,//前面的点 左下
            r,r,r,  //前面的点 右上
            r,-r,r,  //前面的点 右下

            -r,r,-r, //背面的点 左上
            -r,-r,-r,//前面的点 左下
            r,r,-r,  //前面的点 右上
            r,-r,-r  //前面的点 右下
        };
        //顶点的索引顺序
        byte[] indeces = new byte[]{
                //画线时可用
//                0,1,5,4,0,
//                4,6,2,
//                6,7,3,
//                7,5,1,
//                3,2,0
          //画面
            0,1,2,2,1,3,
            4,5,6,6,5,7,
            0,1,4,4,1,5,
            2,3,6,6,3,7,
            4,0,2,4,2,6,
            5,1,3,5,3,7

        };
        //颜色缓冲区
        float[] colors = new float[]{
            0,1f,1f,1f,
            0,1f,0,1f,
            1,1f,1f,1f,
            1,1f,0f,1f,
            0,0,1f,1f,
            0,0,0,1f,
            1,0,1f,1f,
            1,0,0,1f,
        };
//        //设置绘制颜色
//        gl.glColor4f(1,0,0,1);
        //使用颜色缓冲区 值4表示4个值表示一个颜色，0跨度
        gl.glColorPointer(4,GL10.GL_FLOAT,0,BufferUtil.arr2ByteBuffer(colors));
        //设置顶点坐标
        gl.glVertexPointer(3, GL10.GL_FLOAT, 0, BufferUtil.arr2FloatBuffer(coords));
        /**
         * gl.glDrawElements 用于绘制 3D 图形
         * mode：绘制的模式，可以是 GL_POINTS、GL_LINE_STRIP、GL_LINE_LOOP、GL_LINES、GL_TRIANGLE_STRIP、GL_TRIANGLE_FAN 和 GL_TRIANGLES。
         * count：绘制的顶点数。
         * type：顶点索引的数据类型，可以是 GL_UNSIGNED_BYTE、GL_UNSIGNED_SHORT 和 GL_UNSIGNED_INT。
         * indices：指向索引数组的指针。
         */
//        gl.glDrawElements(GL10.GL_LINE_STRIP, indeces.length, GL10.GL_UNSIGNED_BYTE,BufferUtil.arr2ByteBuffer(indeces));
        gl.glDrawElements(GL10.GL_TRIANGLE_STRIP, indeces.length, GL10.GL_UNSIGNED_BYTE,BufferUtil.arr2ByteBuffer(indeces));

        //        //删除纹理 防止内存溢出
//        gl.glDeleteTextures(0,texureId,0);
//        gl.glFlush();
    }
}
