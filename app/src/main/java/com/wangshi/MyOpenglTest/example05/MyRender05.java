package com.wangshi.MyOpenglTest.example05;

import android.opengl.GLU;

import com.wangshi.MyOpenglTest.AbstractRender;
import com.wangshi.MyOpenglTest.BufferUtil;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 *  模板缓冲区测试
 *  相当于使用矩阵实现绘制不同的物体.动画效果，需要硬件支持GPU编程
 */
public class MyRender05 extends AbstractRender {

    public float yrotate=0;
    public float xrotate=0;
    private ArrayList<Float> vertexList;//顶点坐标
    float ratio = 0;
    public float left=-ratio,top=1f,width = 0.3f;
    private boolean xadd=false;
    private boolean yadd=false;
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //设置清屏色，即绘图的背景色，一般设置为黑色,取值都是0-1.
        gl.glClearColor(0,0,0,1);
        //设置清除模板缓冲区的值
        gl.glClearStencil(0);
        //启用顶点缓存，不启用会不显示
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        //启用深度测试
        gl.glEnable(GL10.GL_DEPTH_TEST);//启用后被挡住的物体看不见，否则能看到物体背后的物体
        //启用模板缓冲区
        /**
         * 模板测试函数
         * gl.glStencilFunc(func,ref,mask);在调用之前必需先调用 gl.glClearStencil();
         * func：测试函数,可取下面的值:GL10.GL_NEVER(从不通过)，GL10.GL_ALWAYS(总是通过)，GL10.GL_LESS(A<B时通过),
         * GL10.GL_LEQUAL(A<=B时通过),GL10.GL_EQUAL(A=B时通过),GL10.GL_GEQUAL(A>=B时通过),GL10.GL_GREATER(A>B时通过)
         * GL10.GL_NOTEQUAL(A!=B时通过)
         * ref:参考值
         * mask:设置一个掩码，它将会与参考值和储存的模板值在测试比较它们之前进行与(AND)运算。初始情况下所有位都为1。
         * 模板缓冲的深度在1-8位之间，传递给reg或mask的值，超过了有效深度，那么它们会被截断，只使用那些位于低位的有效位
         *
         * 进入模板缓冲区的值取决于glStencilOp是如何被调用的。
         * fail:模板测试失败采取的行为
         * zfail:模板测试通过，但深度测试失败时采取的行为。
         * zpass:模板测试和深度测试都通过时采取的行为
         * gl.glStencilOp(fail,zfail,zpass);这些值告诉opengl,如果模板测试失败，它应如何修改模板缓冲区的值
         * fail 模板值测试失败，zfail 深度值测试失败，zpass深度值测试通过
         * 这些参数的合法值包括:GL10.GL_KEEP(保持当前值),GL10.GL_ZERO(把它设置为0),GL10.GL_REPLACE(用参考值代替，取自glStencilFunc),
         * GL10.GL_INCR(增加这个值),GL10.GL_DECR(减少这个值),GL10.GL_INVERT(反转这个值),GL10.GL_INCR_WRAP(循环增加这个值)
         * GL10.GL_DECR_WRAP(循环减少这个值)
         */
        gl.glEnable(GL10.GL_STENCIL_TEST);
    }
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        /**
         * 设置视口，即视图的窗口,一般取sufaceview的宽和高,即输出画面的区域
         * 也可以设置，width，height以外的宽高
         */
        gl.glViewport(0,0,width,height);
        //设置投影矩阵模式（必须设置平截投体前设置） 1.透视投影，远小近大，有深度 2.正交投影 与远近无关，远近都一样大
        gl.glMatrixMode(GL10.GL_PROJECTION);
        //加载单位矩阵
        gl.glLoadIdentity();
        //设置平截投体，即视景体。摄像机的可观测区域空间
        ratio = (float)width/(float)height;//为了显示渲染后不失真，保持和视口宽高比例相同
        /**
         * 一般把平截投体的中心作为原点，如果是在一个平面上显示，可将原点放在近景面，方便计算绘制图像在平面中的位置
         *  float left,
         *  float right,
         *  float bottom,
         *  float top,
         *  float zNear 近景面 距离摄像机位置
         *  float zFar 远景面 距离摄像机位置
         */
        gl.glFrustumf(-1*ratio,1*ratio,-1,1,3,7);
    }
    @Override
    public void onDrawFrame(GL10 gl) {
        //清除颜色缓存区,清除深度测试缓存区,清除模板缓存区
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT|GL10.GL_DEPTH_BUFFER_BIT|GL10.GL_STENCIL_BUFFER_BIT);
        //设置着色模式 默认是smooth.着色模式有两种smooth(平滑的)和flat(单调的)
        gl.glShadeModel(GL10.GL_FLAT);
        //模型视图矩阵
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        //加载单位矩阵
        gl.glLoadIdentity();
        /**
         *摄像机位置
         gl – a GL10 interface
         相机位置
         eyeX – eye point X
         eyeY – eye point Y
         eyeZ – eye point Z
         相机观察点（目标点坐标,平截投体的中心点坐标）
         centerX – center of view X
         centerY – center of view Y
         centerZ – center of view Z
         相机方向 ，以免拍偏
         upX – up vector X
         upY – up vector Y
         upZ – up vector Z
         */
        GLU.gluLookAt(gl,0,0,5,0,0,0,0,1,0);

        /******************************绘制白色螺旋线**************************************/
        gl.glPushMatrix();
        gl.glRotatef(xrotate,1,0,0);//绕x轴旋转
        gl.glRotatef(yrotate,0,1,0);//绕y轴旋转
        vertexList = new ArrayList<>();
        float x=0,y=0.8f,z=0,ystep=0.005f,r=0.7f;
        for(float angle=0;angle<(Math.PI*2*3);angle+=(Math.PI/40)){
            x = (float)(r*Math.cos(angle));
            z = -(float)(r*Math.sin(angle));
            y = y-ystep;
            vertexList.add(x);
            vertexList.add(y);
            vertexList.add(z);
        }
        ByteBuffer fvb = BufferUtil.list2ByteBuffer(vertexList);
        //绘制白色直线，设置模板函数，所有操作都不能通过测试，但是对模板缓冲区的值进行增加
        gl.glStencilFunc(GL10.GL_EQUAL,1,0);
        //进入模板缓冲区的值取决于glStencilOp是如何被调用的。
        gl.glStencilOp(GL10.GL_INCR,GL10.GL_INCR,GL10.GL_INCR);
        //绘制白色螺旋线
        gl.glColor4f(1,1,1,1);
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,fvb);
        //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
        gl.glDrawArrays(GL10.GL_LINE_STRIP,0,vertexList.size()/3);//除以3，是因为3个float表示一个点
        gl.glPopMatrix();
        /******************************绘制红色方块**************************************/

        if(xadd){
            left = left+0.01f;
        }else{
            left = left-0.01f;
        }
        if(left<-ratio){
            xadd = true;
        }
        if(left>=ratio-width){
            xadd = false;
        }
        if(yadd){
            top = top+0.01f;
        }else{
            top = top-0.01f;
        }
        if(top>=1){
            yadd= false;
        }
        if(top<-1+width){
            yadd = true;
        }
        float[] rectVertex = {left,top-width,2f,left,top,2f,
        left+width,top-width,2f,left+width,top,2f};
        ByteBuffer vbb = BufferUtil.arr2ByteBuffer(rectVertex);
        //设置模板函数，所有操作都不能通过测试，但是对模板缓冲区的值进行增加
        gl.glStencilFunc(GL10.GL_EQUAL,1,0);
        //进入模板缓冲区的值取决于glStencilOp是如何被调用的。
        gl.glStencilOp(GL10.GL_KEEP,GL10.GL_KEEP,GL10.GL_KEEP);
        //绘制红色方块
        gl.glColor4f(1,0,0,1);
        //设置顶点指针 参数：3表示三维坐标系，GL10.GL_FLOAT 数据类型,0 表示跨度
        gl.glVertexPointer(3,GL10.GL_FLOAT,0,vbb);
        //GL10.GL_POINTS 绘制点,first 起始位置，count 点的数量
        gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP,0,rectVertex.length/3);//除以3，是因为3个float表示一个点
    }
}
